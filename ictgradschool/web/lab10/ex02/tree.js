"use script";

// function makePageInteractive() {
//     var container = document.getElementById("container");
//     var baubles = container.getElementsByClassName("bauble");
//     var i = -1;

//     for (i = 0; i < baubles.length; i++) {

//         baubles[i].onmouseover = function () {

//             // Add the "fall" class to the bauble.
//             this.classList.add("fall");
//         }
//     }
// }

// // Assign an event handler so that the makePageInteractive function is called when the window has loaded.
// window.onload = makePageInteractive;

$(function(){
    $(".bauble").each( function(){
        $(this).mouseover(function(){
            $(this).addClass("fall");
        });
    });
});

// make all fall same time
// $(function(){
//     $(".bauble").mouseover(function(){
//              $(".bauble").addClass("fall");
//         });
// });