"use strict";

var bodyElement = document.body;
var divs = bodyElement.getElementsByTagName("div");
// divs[0].style.backgroundColor = "pink";

$(".text1").css("background-color", "pink");

/* 
 //One way to get the last p and make it red:
 var paragraphs = bodyElement.getElementsByTagName("p");
 lastParagraph = paragraphs[paragraphs.length-1];
 lastParagraph.style.color="red";
 */

//Another way, better:
// var lastParagraph = document.getElementById("footer"); //Note, getElementById is a method of the document node, not a method of any element nodes!!!
// lastParagraph.style.color = "red";

$("#footer").css ("color", "red");


// divs[1].style.visibility = "hidden";

$(".text2").hide();
/*
 // Another way. But unlike setting visibility, this way does not preserve the space allocated to the div (the 'block' display of divs):
 divs[1].style.display="none";
 */

// One way:
// we get back a list of all elements with class=subtitle, even though the list returned only happens to have one item this time.
// var subtitles = bodyElement.getElementsByClassName("subtitle");
// // // get the first subtitle, get its first child (<small>), get its first child which is a textnode and then the latter's value
// subtitles[0].firstChild.nodeValue = "Hello World";

$(".subtitle").text ("Hello World");

// var myButton = document.getElementById("makeBold");
// var myButtons = bodyElement.getElementsByTagName("button");
// console.log(myButtons[0]);

$("#makeBold").click (function() {
    $("p").css("font-weight", "bold" );


});

// When you click the button, set the style for all paragraphs. Accomplished without looping,
// uses createElement() to create a <style> element. See http://www.w3schools.com/jsref/dom_obj_style.asp
// myButton.addEventListener("click", function () {
//     var styleElement = document.createElement("STYLE");
//     styleElement.innerHTML = "p {font-weight: bold}";
//     document.head.appendChild(styleElement);
// });

